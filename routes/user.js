const express = require("express");
router  = express.Router();
const sql    = require("../sql/index");
const user   = require("../sql/col/user");
const md5    = require("md5");    // md5 加密
const bcrypt = require("bcryptjs");         // bcrypt加密
const uuid   = require("node-uuid");        // id生成器
const utils  = require("./../utils/index"); // 验证码
const jwk  = require('jsonwebtoken');
const multer = require('multer')
const upload = multer({dest: 'uploads/'})
const fs     = require('fs')

/** 产品相关的接口 */
/**
 * @api {post} /routes/user/register 注册接口
 * @apiDescription 列表
 * @apiName /routes/user/register
 * @apiGroup user
 * @apiSuccess {json} data
 * @apiParam  {Number} tel 手机号
 * @apiParam  {String} username 用户名
 * @apiParam  {String} password 密码
 * @apiSuccessExample {json} Success-Response:
 * // 注册成功
 * {
 *  code: '10002',
 *  msg: '注册成功'
 * }
 * // 注册失败
 * {
 *    code: '10003',
 *    msg: '注册失败',
 *  }
 * @apiSampleRequest http://localhost:3000/routes/user/register
 * @apiVersion 1.0.0
 */


router.post("/register", ( req, res, next ) => {
    
    let { tel, username, password} = req.body;
    // md5加密
    // password = md5(password);

    // bcrypt 加密
    // 设置密码强度
    let salt = bcrypt.genSaltSync(10);
    
    password = bcrypt.hashSync( password, salt);
    
    // 验证密码，新密码不需要加密，直接与加密的密码进行比较
    // bcrypt.compareSync(newPassword,password);

    // 准备插入的数据
    // 最好补全其他的字段,
    let insertData = {
        userid: "user_" + uuid.v1(),
        username,
        password,
        tel,
        birthday:"",
        sex: -1,
        saving: 0,
        "e-mail":"",
        headerimg: 'https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike92%2C5%2C5%2C92%2C30/sign=62d46c39067b020818c437b303b099b6/d4628535e5dde7119c3d076aabefce1b9c1661ba.jpg'
    }
    
    sql.find(user, { username }, {}).then( data => {
        if(data.length == 0){
            sql.insert( user, insertData ).then( () => {
                res.send({
                    code:10002,
                    msg:"注册成功",
                                
                }).catch( (err) => {
                    console.log(err);
                    res.send({
                        code:"10003",
                        msg:"注册失败",
                    })
                })
            })
        }else {
            res.send({
                code:"10001",
                msg:"用户名已被注册"
            })
        }
    })

})

/**
 * @api {get} /routes/user/sendCode 验证码接口
 * @apiDescription 验证码
 * @apiName /routes/user/sendCode
 * @apiGroup user
 * @apiSuccess {json} data
 * @apiParam  {Number} tel 手机号
 * @apiSuccessExample {json} Success-Response:
 * // 发送验证码成功
 * {
 *  msg: '发送验证码成功'
 *  data: randomCode
 * }
 * // 发送验证码失败
 * {
 *    msg: '发送验证码失败',
 *  }
 * @apiSampleRequest http://localhost:3000/routes/user/sendCode
 * @apiVersion 1.0.0
 */

// 验证码接口
router.get("/sendCode", ( req, res, next) => {
    
    let { tel } = req.query;
    // 调用 utils验证码功能

    // 生成5位验证码
    let randomCode = parseInt(Math.random() * 90000 + 10000);

    utils.sendCode( tel, randomCode).then( () => {
        res.send({
            msg:"发送验证码成功", 
            data:randomCode
        })
    }).catch( err => {
        res.send({
            msg:"发送失败",
            err
        })
    })
})

// router.get("/sendCode", ( req, res, next) => {
    
//   let { tel } = req.query;

//   sql.find( user, { tel }, { _id:0 }).then( data => {
//       if( data.length == 0 ) {
//           // 调用 utils验证码功能

//           // 生成5位验证码
//           let randomCode = parseInt(Math.random() * 90000 + 10000);

//           utils.sendCode( tel, randomCode).then( () => {
//               res.send({
//                   msg:"发送验证码成功", 
//                   data:randomCode
//               })
//           }).catch( err => {
//               res.send({
//                   msg:"发送失败"
//               })
//           })
           
//       } else {
//           // 用户存在
//           res.send({
//               code:10001,
//               msg:"该用户已存在",
//               tel:tel
//           })
//       }
//   })
// })

/**
 * @api {post} /routes/user/login 登录接口
 * @apiDescription 列表
 * @apiName /routes/user/login
 * @apiGroup user
 * @apiSuccess {json} data
 * @apiParam  {Number} tel 手机号
 * @apiParam  {String} password 密码
 * @apiSuccessExample {json} Success-Response:
 * // 登录成功
 * {
 *  code: '10004',
 *  msg: '登录成功'
 *  data:{
 *      // 这里是用户数据
 *      }
 * }
 * // 密码错误
 * {
 *    code: '10005',
 *    msg: '密码错误',
 *  }
 * // 用户未注册
 * {
 *    code: '10006',
 *    msg: '密码错误',
 *  }
 * @apiSampleRequest http://localhost:3000/routes/user/login
 * @apiVersion 1.0.0
 */

router.post("/login", ( req, res, next ) => {
    
    let { tel, password } = req.body;

    sql.find( user, { tel }, { _id:0 }).then( data => {

        if( data.length == 0) {
            res.send({
                code:"10006",
                msg:"该用户未注册"
            })

        }else {
            
            // let salt = bcrypt.genSaltSync(10);
            // let newPassword = bcrypt.hashSync( password, salt);

            let pwd = data[0].password;
            // 比较密码,不用对新密码加密，直接和加密的密码比较即可
            let result = bcrypt.compareSync( password, pwd);

            if( result ) {
                // token
                let token = jwk.sign( { tel }, 'nicis', { expiresIn: 60 * 60 * 24 } ) // 授权24小时

                // 登录成功后,返回用户信息
                res.send({
                    code:"10004",
                    msg:"登录成功",
                    data:{
                        useid: data[0].userid,
                        username:data[0].username,
                        nickname:data[0].nickname,
                        token
                    }
                })
            }else {
                res.send({
                    code:"10005",
                    msg:"密码错误",
                })
            }
        }
    })
})
