var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var usersRouter = require('./routes/users')

var booknameRoyter = require('./routes/bookname')
var bookdetailRoyter = require('./routes/bookdetail')



//后台接口的路由
var usersApi = require('./api/users');





var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  next();
}
app.use(allowCrossDomain);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//这个是web文件下的托管
app.use('/uploads', express.static(__dirname + '/uploads'))
app.use(cookieParser());
app.use(session({
secret: '12345',
name: 'name',
cookie: {maxAge: 60000},
resave: false, 
saveUninitialized: true,
}));


app.use('/api/users',usersRouter)


app.use('/bookname',booknameRoyter)
app.use('/bookdetail',bookdetailRoyter)

app.use('/api/users', usersApi);

 


var allowCors = function(req, res, next) {

    res.header('Access-Control-Allow-Origin', req.headers.origin);
  
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  
    res.header('Access-Control-Allow-Headers', 'Content-Type');
  
    res.header('Access-Control-Allow-Credentials','true');
  
    next();
  
  };
  
  app.use(allowCors);
  
 
// catch 404 and forward to error handler

app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
