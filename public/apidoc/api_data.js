define({ "api": [
  {
    "type": "",
    "url": "{}获取对应小说(有跨域)",
    "title": "",
    "description": "<p>获取对应小说(有跨域)</p>",
    "name": "___id______",
    "group": "________id",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": \"200\",\n  \"meassge\": \"查询成功\",\n  \"data\": [\n    ........\n         ]\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "https://novel.juhe.im/book/12345"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/wwwwww.js",
    "groupTitle": "________id"
  },
  {
    "type": "get",
    "url": "/bookdetail/find",
    "title": "获取分类详情",
    "description": "<p>获取分类详情</p>",
    "name": "____",
    "group": "______bookdetail_find",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": \"200\",\n  \"meassge\": \"查询成功\",\n  \"data\": [\n  \"_id\": \"5e1351c8321242780c54b0ab\",\n  \"__v\": 0,\n \"其他类别\":{}，\n \"侦探_推理\":{},\n \"玄幻_仙侠\":{}，\n  ....................\n         ]\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:3000/bookdetail/find"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/bookdetail.js",
    "groupTitle": "______bookdetail_find"
  },
  {
    "type": "get",
    "url": "/bookname/search",
    "title": "搜索查询",
    "description": "<p>获取搜索信息</p>",
    "name": "__",
    "group": "____bookname_search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "text",
            "description": "<p>搜索单词</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n \"code\": \"200\",\n  \"meassge\": \"查询成功\",\n  \"data\": [\n    ........\n         ]\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:3000/bookname/search"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/bookname.js",
    "groupTitle": "____bookname_search"
  },
  {
    "type": "post",
    "url": "/admin/add",
    "title": "验证注册信息",
    "description": "<p>逻辑正确执行注册</p>",
    "name": "adduser",
    "group": "adduser",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>参数必填(手机号)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>参数必填(密码)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>参数必填(用户名)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "data:{\nmessage:'验证码发送成功',\ncode:'6666',\nuserid: '唯一标识',\ntoken: token\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:3000/admin/add"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/admin.js",
    "groupTitle": "adduser"
  },
  {
    "type": "post",
    "url": "/admin",
    "title": "注册页面获取验证码",
    "description": "<p>获取验证码</p>",
    "name": "code",
    "group": "code",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>参数必填(手机号)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "data:{\nmessage:'验证码发送成功',\ncode:验证码,\ntel：tel\n}\ndata:{\n code:'3333',\n message:'已注册'\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:3000/admin"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/admin.js",
    "groupTitle": "code"
  }
] });
