
const sql = {
    insert(name,nav) { 
        return new Promise((yes, no) => {
            name.insertMany(nav, (err) => {
                if (err) throw err;
                yes()
            })
        })
    },
    delete(name, nav, dateType) { 
        dateType = dateType || 'deleteOne'
        return new Promise((yes,no) => {
            name[dateType](nav, (err) => {
                if (err) throw err;
                yes()
            })
        })
    },
    update(name, nav, main, dateType) {
        dateType = dateType || 'updateOne'
        return new Promise((yes, no) => {
            name[dateType](nav, main, (err) => {
                if (err) throw err;
                yes()
            })
        })
    },
    paging (name,nav, main, limitNum, pageCode) {
        return new Promise((resolve, reject) => {
          // limit(limitNum) 每页显示个数
          // skip(limitNum * pageCode) // 每页从哪一个开始
          name.find(nav, main).limit(limitNum).skip(limitNum * pageCode).exec((err, data) => {
            if (err) throw err;
            resolve(data)
          })
        })
    },
    distinct(CollectionName, name) {
        return new Promise((resolve, reject) => {
          CollectionName.distinct(name).exec((err, data) => {
            if (err) throw err;
            resolve(data)
          })
        })
      },
    find(name, nav, main) {
        return new Promise((yes, no) => {
            nav = nav || {}
            main= main ||{}
            name.find(nav, main, (err, date) => {
                if (err) {}
                 else{yes(date)}   
                
                
           })
       })
    }  
}
module.exports = sql;