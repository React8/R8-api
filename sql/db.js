const mongoose = require('mongoose');//引入模块
const Dburl = 'mongodb://localhost:27017/book';

mongoose.connect(Dburl, { useNewUrlParser: true });

mongoose.connection.on('connected', () => {
    console.log('成功')
})
mongoose.connection.on('disconnected', () => {
    console.log('断开')
})
mongoose.connection.on('error', () => {
    console.log('错误')
})

module.exports=mongoose//暴漏出去