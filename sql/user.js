const mongoose = require("./db");

const Schema   = mongoose.Schema;

let schema = new Schema({
    userid:{ type: String }, 
    username:{ type: String }, 
    password:{ type: String }, 
    nickname:{ type: String }, 
    birthday:{ type: String }, 
    sex:{ type: Number }, 
    tel:{ type: Number },
    email:{ type: String },
    saving: {type: Number},
    headerimg: {type: String}
},{ versionKey: 0});

module.exports = mongoose.model("user",schema);