const mongoose = require('./db.js');//引入数据库连接模块

const Schema = mongoose.Schema;//拿到数据库对应的集合对象

const bookNameSchema = new Schema({
            _id: {type:String},
            author: {type:String},
            cover: {type:String},
            longIntro: {type:String},
            title: {type:String},
            zt: {type:String},
            cat: {type:String},
            wordCount: {type:String},
            retentionRatio: {type:String},
            followerCount: {type:String},
            updated: {type:String},
            chaptersCount: {type:String},
            lastChapter: {type:String}


})//设计用户集合的字段以及数据类型


module.exports = mongoose.model('bookname', bookNameSchema);
  